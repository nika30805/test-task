FROM node:16.18.1-alpine

WORKDIR /app

RUN apk add --no-cache bash

#Depends
COPY ./package.json ./
COPY ./yarn.lock ./

RUN yarn install --frozen-lockfile

#BUILD
COPY ./i18n ./i18n/
COPY ./docs ./docs/
COPY ./src ./src/
COPY ./static ./static/
COPY babel.config.js ./
COPY docusaurus.config.js ./
COPY sidebars.js ./

RUN yarn build-ci

#Команда для запуска сервера внутри контейнера
CMD [ "yarn", "global-stand" ]
