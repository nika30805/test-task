---
sidebar_position: 3
---

# Техническое задание
В тестовом задании вам будет предложено отправить запрос к API. Задание вы можете найти по ссылке.
Текст вашего запроса введите, пожалуйста, в поле ниже и прикрепите XML-файл с полученным ответом.

API-ключ Яндекс.Словаря:
dict.1.1.20230113T114205Z.bb5935a8a6549795.b6c675a214b2695ae5086f4d1a2c2f8d7562c286

### Запрос XML:
curl 'https://dictionary.yandex.net/api/v1/dicservice/lookup?text=sky&key=dict.1.1.20230113T114205Z.bb5935a8a6549795.b6c675a214b2695ae5086f4d1a2c2f8d7562c286&lang=en-ru'

### Ответ в формате XML
```XML
<?xml version="1.0" encoding="utf-8"?>
<DicResult><head /><def pos="noun" ts="skaɪ"><text>sky</text><tr pos="noun" gen="ср" fr="10"><text>небо</text><syn pos="noun" fr="5"><text>небеса</text></syn><syn pos="noun" gen="ср" fr="1"><text>поднебесье</text></syn><mean><text>heaven</text></mean><mean><text>heavens</text></mean><ex><text>night sky</text><tr><text>ночное небо</text></tr></ex><ex><text>clear blue sky</text><tr><text>чистое голубое небо</text></tr></ex><ex><text>cloudless skies</text><tr><text>безоблачные небеса</text></tr></ex></tr><tr pos="noun" gen="м" fr="1"><text>небосвод</text><syn pos="noun" gen="м" fr="1"><text>небосклон</text></syn><syn pos="noun" fr="1"><text>небесный свод</text></syn><mean><text>firmament</text></mean><mean><text>horizon</text></mean><ex><text>eastern sky</text><tr><text>восточный небосклон</text></tr></ex></tr><tr pos="noun" gen="м" fr="5"><text>Скай</text></tr></def><def pos="adjective" ts="skaɪ"><text>sky</text><tr pos="adjective" fr="5"><text>небесный</text><mean><text>heavenly</text></mean><ex><text>sky harbor</text><tr><text>небесная гавань</text></tr></ex></tr></def></DicResult>
```

### Запрос Json:
curl 'https://dictionary.yandex.net/api/v1/dicservice.json/lookup?text=sky&key=dict.1.1.20230113T114205Z.bb5935a8a6549795.b6c675a214b2695ae5086f4d1a2c2f8d7562c286&lang=en-ru'

### Ответ в формате JSON
```javascript 
{"head":{},"def":[{"text":"sky","pos":"noun","ts":"skaɪ","tr":[{"text":"небо","pos":"noun","gen":"ср","fr":10,"syn":[{"text":"небеса","pos":"noun","fr":5},{"text":"поднебесье","pos":"noun","gen":"ср","fr":1}],"mean":[{"text":"heaven"},{"text":"heavens"}],"ex":[{"text":"night sky","tr":[{"text":"ночное небо"}]},{"text":"clear blue sky","tr":[{"text":"чистое голубое небо"}]},{"text":"cloudless skies","tr":[{"text":"безоблачные небеса"}]}]},{"text":"небосвод","pos":"noun","gen":"м","fr":1,"syn":[{"text":"небосклон","pos":"noun","gen":"м","fr":1},{"text":"небесный свод","pos":"noun","fr":1}],"mean":[{"text":"firmament"},{"text":"horizon"}],"ex":[{"text":"eastern sky","tr":[{"text":"восточный небосклон"}]}]},{"text":"Скай","pos":"noun","gen":"м","fr":5}]},{"text":"sky","pos":"adjective","ts":"skaɪ","tr":[{"text":"небесный","pos":"adjective","fr":5,"mean":[{"text":"heavenly"}],"ex":[{"text":"sky harbor","tr":[{"text":"небесная гавань"}]}]}]}]}```



